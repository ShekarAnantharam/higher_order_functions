
let arr=[];
function flatten(elements) {
    // Flattens a nested array (the nesting can be to any depth).
    // Hint: You can solve this using recursion.
    // Example: flatten([1, [2], [3, [[4]]]]); => [1, 2, 3, 4];

    if(!Array.isArray(elements)){
        throw new Error("invalid arguments");
    }
    else{
        for (let index=0;index<elements.length;index++){
            if (Array.isArray(elements[index])){
                
                arr=flatten(elements[index]);
                /*if (elements[i].length==1 && !(Array.isArray(elements[i][0]))){
                arr.push(elements[i][0]);
                }
                else{
                    flatten(elements[i]);
                }*/
            }
            else{
                arr.push(elements[index]);
            }
        }
    }
    return arr;


}

module.exports=flatten;

  //console.log(flatten([1,[2,3,[4,5,[6],7,8],9,10],11,12]));