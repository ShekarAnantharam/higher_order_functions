
function filter(elements, cb) {
    // Do NOT use .filter, to complete this function.
    // Similar to `find` but you will return an array of all elements that passed the truth test
    // Return an empty array if no elements pass the truth test
    let arr=[];
    if(!Array.isArray(elements)|| typeof cb!="function"){
        throw new Error("invalid arguments");
    }
    else{
 
        for(let index=0;index<elements.length;index++){
            cb(elements[index],arr);
        }
    }
    return arr;
}

module.exports=filter;